package im.conversations.compliance.xmpp;
import im.conversations.compliance.xmpp.tests.AbstractTest;
import java.util.Arrays;
import java.util.List;
public class Tests {
public static List<Class<? extends AbstractTest>> getTests() {
return Arrays.asList(im.conversations.compliance.xmpp.tests.InBandRegistrationTest.class
,im.conversations.compliance.xmpp.tests.TurnExternalServiceTest.class
,im.conversations.compliance.xmpp.tests.Bookmark2Conversion.class
,im.conversations.compliance.xmpp.tests.StreamManagement.class
,im.conversations.compliance.xmpp.tests.XmppOverTls.class
,im.conversations.compliance.xmpp.tests.MAM.class
,im.conversations.compliance.xmpp.tests.MamMuc.class
,im.conversations.compliance.xmpp.tests.HttpUpload.class
,im.conversations.compliance.xmpp.tests.EntityCapabilities.class
,im.conversations.compliance.xmpp.tests.AlternateConnections.class
,im.conversations.compliance.xmpp.tests.HttpUploadCors.class
,im.conversations.compliance.xmpp.tests.PEP.class
,im.conversations.compliance.xmpp.tests.AbuseContactTest.class
,im.conversations.compliance.xmpp.tests.VcardMuc.class
,im.conversations.compliance.xmpp.tests.OMEMO.class
,im.conversations.compliance.xmpp.tests.MultiUserChat.class
,im.conversations.compliance.xmpp.tests.CSI.class
,im.conversations.compliance.xmpp.tests.AvatarConversion.class
,im.conversations.compliance.xmpp.tests.MamExtended.class
,im.conversations.compliance.xmpp.tests.Blocking.class
,im.conversations.compliance.xmpp.tests.Push.class
,im.conversations.compliance.xmpp.tests.MessageCarbons.class
,im.conversations.compliance.xmpp.tests.OfflineStorage.class
,im.conversations.compliance.xmpp.tests.RosterVersioning.class
,im.conversations.compliance.xmpp.tests.StunExternalServiceTest.class
,im.conversations.compliance.xmpp.tests.CarbonsRules.class
,im.conversations.compliance.xmpp.tests.Proxy65.class
);
}
}
